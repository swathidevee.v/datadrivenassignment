package com.data;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PassingDataApache {
	public static void main(String[] args) throws IOException {
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		driver.get("https://demowebshop.tricentis.com/");
		File f=new File("/home/swathidevee/Documents/Testdata/Testdata3.xlsx");
		FileInputStream fis=new FileInputStream(f);
		XSSFWorkbook wb=new XSSFWorkbook(fis);
		XSSFSheet sheet=wb.getSheetAt(0);
		
		int  rows=sheet.getPhysicalNumberOfRows();
		for (int i = 1; i < rows; i++) {
			String firstName=sheet.getRow(i).getCell(0).getStringCellValue();
			String lastName =sheet.getRow(i).getCell(1).getStringCellValue();
			String email =sheet.getRow(i).getCell(2).getStringCellValue();
			String password =sheet.getRow(i).getCell(3).getStringCellValue();
			driver.findElement(By.linkText("Register")).click();
			driver.findElement(By.id("gender-male")).click();
			driver.findElement(By.id("FirstName")).sendKeys(firstName);
			driver.findElement(By.id("LastName")).sendKeys(lastName);
			driver.findElement(By.id("Email")).sendKeys(email);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.id("ConfirmPassword")).sendKeys(password);
			driver.findElement(By.id("register-button")).click();
			driver.findElement(By.xpath("//a[contains(text(),'Log out')]")).click();
		}
		

		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
